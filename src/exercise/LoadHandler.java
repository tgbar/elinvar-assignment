package exercise;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class LoadHandler {

    private static final int MAX_PRICE_UPDATES = 10;
    private final exercise.Consumer consumer;
    private Map<Integer, PriceUpdate> priceUpdateMap = Collections.synchronizedMap(new LinkedHashMap<>());
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public LoadHandler(Consumer consumer) {
        this.consumer = consumer;
        updatePrices();
    }

    public void receive(PriceUpdate priceUpdate) {
        priceUpdateMap.put(priceUpdate.hashCode(), priceUpdate);
    }

    /**
     * This method will execute every second, get the lock for the priceUpdateMap object and send it to the consumer
     */
    public void updatePrices() {
        final ScheduledFuture<?> future = scheduler.scheduleAtFixedRate(
                () -> {
                    final List<PriceUpdate> allPriceUpdatesList = new ArrayList<>();

                    //As stated in the official documentation, it is imperative that the user manually synchronize on the returned
                    //map when iterating over any of its collection views.
                    synchronized (priceUpdateMap) {
                        Iterator<Map.Entry<Integer, PriceUpdate>> iterator = priceUpdateMap.entrySet().iterator();
                        //If the number of stocks is bigger than the number of allowed updates per second
                        //get just the MAX_PRICE_UPDATES objects from the map.
                        //In the next execution of this methods, the newest added stocks will be picked.
                        if (priceUpdateMap.size() > MAX_PRICE_UPDATES) {
                            AtomicInteger actAtomicInteger = new AtomicInteger(1);
                            getStocksToUpdate(allPriceUpdatesList, iterator, actAtomicInteger, true);
                        } else {
                            getStocksToUpdate(allPriceUpdatesList, iterator, null, false);
                        }
                    }

                    //TODO implement chunks
                    if (allPriceUpdatesList.size() > 0) {
                        consumer.send(allPriceUpdatesList);
                    }
                },
                5, 1, TimeUnit.SECONDS
        );
    }

    /**
     * Get and remove the picked stocks from the map until reach the MAX_PRICE_UPDATE
     *
     * @param allPriceUpdatesList
     * @param iterator
     * @param i an AtomicInteger used to increment an int inside a lambda expression
     * @param isStocksBiggerThanUpdates
     */
    private void getStocksToUpdate(List<PriceUpdate> allPriceUpdatesList,
                                   Iterator<Map.Entry<Integer, PriceUpdate>> iterator,
                                   AtomicInteger i,
                                   boolean isStocksBiggerThanUpdates) {

        for (; iterator.hasNext(); ) {
            if (isStocksBiggerThanUpdates && i.get() == MAX_PRICE_UPDATES) break;

            Map.Entry<Integer, PriceUpdate> entry = iterator.next();
            allPriceUpdatesList.add(entry.getValue());
            iterator.remove();
            if(i != null) i.incrementAndGet();
        }
    }
}
